# -*- coding: utf-8 -*-
"""
Created on Mon Mar 12 15:31:24 2018

@author: Joana
"""

def initialize(context):
    context.security = symbol('TSLA')
    
    schedule_function(func=action_function,
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_open(hours=0, minutes=1)
                      )
    
def action_function(context, data):
    price_history = data.history(assets=context.security,
                                 fields='price',
                                 bar_count=350,
                                 frequency='1d'
                                 )
       
    average_price = price_history.mean()
   
    current_price = data.current(assets=context.security,
                                fields='price'
                                )
    if data.can_trade(context.security):
        if current_price > average_price * 1.05:
            order_target_percent(context.security, 1)
            log.info('Buying +{}'.format(context.security))
        else:
            order_target_percent(context.security, 0)
            log.info('Setting my position to zero on +{}'.format(context.security.symbol))